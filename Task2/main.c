#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define MAX_GUESS 7

void narisi(int poizkusi);    //Funkcija, ki izriše sliko igralca na vislicah
char *random_beseda(char besede[][10], int stetje_besed);   //Funkcija, ki vrne naključno besedo iz matrike besed
int preveri(char ugib, char *beseda, char *napredek);   //Funkcija, ki preveri, če je igralec uganil črko


int main(){
    //Array besed, med katerimi izbira program
    char besede[][10] = {"igra", "beseda", "computer", "racunalnik", "zmaga", "tablica", "program", "zvocnik"};
    int stetje_besed = sizeof(besede) / sizeof(besede[0]);

    char* beseda = random_beseda(besede, stetje_besed); //Z funkcijo random_beseda izberemo eno izmed besed
    int dolzina_besed = strlen(beseda);     //Z funkcijo strlen dobimo dolzino besede
    
    char napredek[dolzina_besed + 1];      //Spremenljivka, ki shranjuje trenutno stanje napredka
    for (int i = 0; i < dolzina_besed; i++) {   //Črke nadomestimo z _
        napredek[i] = '_';
        }
    napredek[dolzina_besed] = '\0';     //Dodamo nul znak na konec niza napredek

    int nepravilno = 0;
    char ugib;
    char poizkusi[26] = {0};
    int pravilno;


    while (nepravilno < MAX_GUESS) {    //While zanka se ponavlja dokler nepravilnih ugibanj ni enako max. št. poizkusov
    printf("Trenutno stanje: %s\n", napredek);      
    printf("Poskusate lahko se %d-krat. Vnesite crko: ", MAX_GUESS - nepravilno);
    scanf(" %c", &ugib);  
    if ((ugib >= 'a' && ugib <= 'z') || (ugib >= 'A' && ugib <= 'Z')) { //Preverimo če je bil vnešen znak črka
        printf("");
        } else{
            printf("Vnesli ste neveljaven znak. Prosim, vnesite samo crko.\n");
            continue;   //Ponovimo zanko
        }
    

    if (poizkusi[ugib - 'a'] == 1) {    //Preverimo, če je bila črka že poizkušena in ponovimo zanko
        printf("To crko ste ze poskusili. Poskusite z drugo.\n");
        continue;
        }
    poizkusi[ugib - 'a'] = 1;   //Oznacimo, da je uporabnik poizkusil z ugibano crko

    for (int i = 0; i < MAX_GUESS; i++) { //For zanka ki narise nasega hangmana
        if (nepravilno == i) {
            narisi(MAX_GUESS - i);
            break;
        }
    }

    //Izpisemo seznam uganjenih črk
    printf("Uganjene crke: ");
    for (int i = 0; i < 26; i++) {
        if (poizkusi[i] == 1) {
            printf("%c ", i + 'a');
        }
    }   printf("\n");

    //Preverimo, če je ugibana črka pravilna
    pravilno = preveri(ugib, beseda, napredek);
    if (pravilno) {
        printf("Cestitke! Pravilno ste uganili crko.\n");   //Če je, čestitamo
        } else {
        printf("Ojoj! Napacna crka.\n");    //Če ni, opozorimo in povečamo št. nepravilnih poizkusov
        nepravilno++;
        }
        if (strcmp(beseda, napredek) == 0) {    //Funkcija strcmp primerja če sta niza enaka. Če sta, je if stavek resničen.
        printf("Cestitke, zmagali ste! Beseda je bila '%s'.\n", beseda);
        return 0;
        }
    }
    //Ko zmanjka poizkusov, je konec igre.
    printf("Zal vam je zmanjkalo poizkusov. Izgubili ste.\n");
    printf("Pravilna beseda je bila '%s'.\n", beseda);
    return 0;
}

    //Funkcija, ki vrne naključno besedo iz matrike besed
    char *random_beseda(char besede[][10], int stetje_besed){
        //Inicializiramo generator nakljucne besede na podlagi trenutnega časa
        srand(time(NULL));
        //Generiramo naključen index iz intervala [0, stetje besed)
        int index = rand() % stetje_besed;
        return besede[index];   //Vrne naključno izbrano besedo
    }

//Funkcija, ki preveri, če je igralec uganil črko
int preveri(char ugib, char *beseda, char *napredek) {
    int pravilni_ugib = 0;  //Spremenljivka hrani rezultat preverjanja
    //Gremo čez vse črke
    for (int i = 0; i < strlen(beseda); i++) {  //Funkcija strlen dobi dolžino besede
        //Če je ugibana črka pravilna, jo damo na ustrezno mesto v nizu
        if (beseda[i] == ugib) {
        napredek[i] = ugib;
        pravilni_ugib = 1;
        }
    }   return pravilni_ugib;   //Vrnemo rezultat preverjanja
}

//Funkcija, ki izriše sliko igralca na vislicah
void narisi(int poizkusi) {
    switch (poizkusi) {
        case 6:
        printf("\n\n\n\n\n\n\n");
        break;
        case 5:
        printf("\n\n\n\n\n\n_____");
        break;
        case 4:
        printf("\n |\n |\n |\n |\n |\n___|__");
        break;
        case 3:
        printf(" ____\n |\n |\n |\n |\n |\n|");
        break;
        case 2:
        printf(" ____\n |/ |\n |\n |\n |\n |\n|");
        break;
        case 1:
        printf(" ___\n |/ |\n | ()\n |\n |\n |\n|");
        break;
        case 0:
        printf(" ___\n |/ |\n | ()\n | |\n |\n |\n|");
        break;
    }
}