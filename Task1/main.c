//Simulator Dvigala
#include <stdio.h>  

void dvigalo(int trenutno_nadstropje);  //Funkcija za izvajanje dvigala
void shrani_lokacijo(int *trenutno_nadstropje); //Funkcija za shranjevanje lokacije dvigala
void beri_lokacijo(int *trenutno_nadstropje);   //Funkcija za branje prejšnje lokacije dvigala

int main(){
    printf("Pozdravljeni v dvigalu!\n");

    int trenutno_nadstropje = 0;    //Inicializiranje spremenljivke

    beri_lokacijo(&trenutno_nadstropje);    //Preberemo shranjeno lokacijo dvigala

    dvigalo(trenutno_nadstropje);   //Poženemo funkcijo izvajanja dvigala
}

void dvigalo(int trenutno_nadstropje){ 
    //Neskončna zanka while (1), ki se izvaja, dokler ni prekinjena z neko drugo operacijo
    while (1){
        int stevilo_potnikov;   
        char nadstropje[2];     //Spremenljivka za izbrano nadstropje
        printf("Izberi nadstropje (P, 1, 2, 3, 4, 5, B1, B2, B3): ");
        scanf("%s", nadstropje);
        //Validacija vnosa izbranega nadstropja
        int validacija = 0;
        //Preverimo, če je izbrano nadstropje veljavno
        for (int i = 0; i < 9; i++){
            if (nadstropje[0] == "P12345B1B2B3"[i]){
                validacija = 1;
                break;
                }
            }
            //Če ni veljavno, napaka in znova začnemo zanko
            if (!validacija){
                printf("Neveljaven vnos, prosim vnesi: P, 1, 2, 3, 4, 5, B1, B2, B3.\n");
                continue;
                }

        //Pretvori input v intiger
        int nadstropje_int;
        if (nadstropje[0] == 'P'){
        nadstropje_int = 0;
        //Če želimo v klet, pretvorimo nadstropja v negativne številke
        } else if (nadstropje[0] == 'B'){
            //else if zanka, ki preverja drugi znak, če je prvi znak B
            if (nadstropje[1] == '1'){
            nadstropje_int = -1;
            } else if (nadstropje[1] == '2'){
                nadstropje_int = -2;
                } else if (nadstropje[1] == '3'){
                    nadstropje_int = -3;
                    }
        } else{
            nadstropje_int = nadstropje[0] - '0' ;
            }

        //Preveri, če je dvigalo na zaželenem nadstropju
        if (nadstropje_int == trenutno_nadstropje){
            printf("Na tem nadstropju ste trenutno.\n");
            continue;
        }

        //Preveri če hoče uporabnik gor ali dol
        //Če je želeno nadstropje višje od trenutnega, se povzpenjamo
        if (nadstropje_int > trenutno_nadstropje){
            printf("Povzpenjamo se...\n");
            //for stavek se izvaja dokler ne dosežemo želenega nadstropja
            for (int i = trenutno_nadstropje; i <= nadstropje_int; i++){
                printf("Nadstropje %d\n", i);
                trenutno_nadstropje = nadstropje_int;
            } 
            printf("Dosegli smo zeljeno nadstropje.\n");
            //Spremenljivo število potnikov
            printf("Vnesite novo stevilo potnikov: ");
            scanf("%i", &stevilo_potnikov);
            printf("Stevilo potnikov: %i\n", stevilo_potnikov);

        } else {
            //else (želeno nadstropje nižje od trenutnega), se spuščamo
                printf("Spuscamo se...\n");
                //for stavek se izvaja dokler ne dosežemo želenega nadstropja
                for (int i = trenutno_nadstropje - 1; i >= nadstropje_int; i--){
                    printf("Nadstropje %d\n", i);
                    trenutno_nadstropje = nadstropje_int;
                } 
                printf("Dosegli smo zeljeno nadstropje.\n");
                //Spremenljivo število potnikov
                printf("Vnesite novo stevilo potnikov: ");
                scanf("%i", &stevilo_potnikov);
                printf("Stevilo potnikov: %i\n", stevilo_potnikov);

            }
            //Pokličemo funkcijo shrani_lokacijo, da shranimo trenutno nadstropje
            shrani_lokacijo(&trenutno_nadstropje);
        
        //Vprašamo uporabnika, če želi ostati ali oditi z dvigala
        char izbira[1];
        printf("Ali zelite ostati na dvigalu (J/N) ? ");
        scanf("%s", izbira);
        //Neskončna while zanka, ki se izvaja, dokler ni prekinjena
        while (1){
            //Preveri veljavnost vnosa
            if (izbira[0] == 'J' || izbira[0] == 'N'){
                break;
                } 
                //else stavek, ki se izvede, če je bilo vnešeno karkoli drugega razen J ali N
                else{
                    printf("Napacen vnos! J - ja, N - ne.\n");
                    printf("Ali zelite ostati na dvigalu (J/N) ? ");
                    scanf("%s", izbira);
                    }
            }
            //Če vnesemo N, se program zaključi
            if (izbira[0] == 'N'){
                break;
            }
    }
}
    //Uporabljeno shranjevanje podatkov v datoteko
    void shrani_lokacijo(int *trenutno_nadstropje){
        //Odpri txt datoteko za pisanje
        FILE* f = fopen("lokacija.txt", "w");
        //Preveri, če se je datoteka pravilno odprla
        if(f == NULL){
            printf("Error: Datoteke ni mogoče odpreti!\n");
        }
        //Zapiši trenutno nadstropje v datoteko
        fprintf(f, "%d", *trenutno_nadstropje);
        //Zapri datoteko
        fclose(f);
    }

    void beri_lokacijo(int *trenutno_nadstropje) {
        //Odpri txt datoteko za branje
        FILE* f = fopen("lokacija.txt", "r");
        //Preveri, če se je datoteka pravilno odprla
        if(f == NULL) {
            printf("Error: Datoteke ni mogoče odpreti!\n");
            }
        //Preberi trenutno nadstropje iz datoteke
        fscanf(f, "%d", trenutno_nadstropje);
        //Zapri datoteko
        fclose(f);
    }